package com.cgi.connect;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.source.SourceTask;
import org.apache.plc4x.java.api.exceptions.PlcConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PLCSubscriptionSourceTask extends SourceTask {
  private static final Logger log = LoggerFactory.getLogger(PLCSubscriptionSourceTask.class);

  private PlcSubscriptionFetcher plcFetcher;
  private PLCSubscriptionSourceConfig config;
  private Integer pollInterval;

  public PLCSubscriptionSourceTask() {}

  @Override
  public String version() {
    return VersionUtil.getVersion();
  }

  /**
   * Task initialisation from configuration
   *
   * @param lconfig the map of properties
   */
  @Override
  public void start(Map<String, String> lconfig) {
    this.config = new PLCSubscriptionSourceConfig(lconfig);
    pollInterval = this.config.getInt(PLCSubscriptionSourceConfig.PLC_POLL_INTERVAL);

    plcFetcher = new PlcSubscriptionFetcher();

    try {
      plcFetcher.configure(config);
    } catch (PlcConnectionException e) {
      log.error("The Plc Server could not be reached, review your configuration.", e);
      throw new IllegalStateException(
          "The Plc Server could not be reached, review your configuration.");
    }
  }

  /**
   * Kafka connect call this method when it check for more input data
   *
   * @return the list of record to publish to Kafka
   */
  @Override
  public List<SourceRecord> poll() {
    plcFetcher.maybeSendFetches();
    try {
      List<SourceRecord> newEvents = plcFetcher.getNewEvents();
      if (newEvents.isEmpty()) {
        Thread.sleep(pollInterval);
      }
      return newEvents;
    } catch (InterruptedException e) {
      return Collections.emptyList();
    }
  }

  @Override
  public void stop() {
    log.info("Stopping the task...");
    plcFetcher.close();
    log.info("Scheduled Runner stopped");
  }
}
