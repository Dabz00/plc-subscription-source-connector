package com.cgi.connect;

import static com.cgi.connect.PLCSubscriptionSourceConfig.*;

import com.cgi.connect.config.ConfigExtractor;
import com.cgi.connect.config.ConnectionManager;
import com.cgi.connect.converter.ResponseToRecordConverter;
import com.cgi.connect.converter.SchemaGenerator;
import com.cgi.connect.converter.TreeElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.plc4x.java.api.PlcConnection;
import org.apache.plc4x.java.api.exceptions.PlcConnectionException;
import org.apache.plc4x.java.api.messages.PlcReadResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Scheduled runner holding the business logic */
public class PlcSubscriptionFetcher {
  private static final Logger log = LoggerFactory.getLogger(PlcSubscriptionFetcher.class);

  private String kafkaTopic;
  private List<String> keyComposition;
  private LinkedBlockingQueue<SourceRecord> eventQueue;
  private Map<String, Object> valueBuffer;
  private List<String> subscriptions;
  private Map<String, List<Pair<String, String>>> mappings;
  private Map<String, String> subscriptionPath;
  private PlcConnection plcConnection;
  private Schema outputSchema;
  private Map<String, TreeElement> fieldTreeElements;
  private Map<String, Boolean> inflightMap;

  public void configure(PLCSubscriptionSourceConfig config) throws PlcConnectionException {
    // retrieve and init configuration values
    this.kafkaTopic = config.getString(KAFKA_TOPIC);
    this.subscriptions = config.getList(SUBSCRIPTIONS);
    this.keyComposition = config.getList(OUTPUT_KEY);
    var plcConnectionString = config.getString(PLC_CONNECTION_STRING);

    this.inflightMap = new ConcurrentHashMap<>();
    this.eventQueue = new LinkedBlockingQueue<>();
    // Contains path for each configured subscription
    this.valueBuffer = new HashMap<>();
    mappings = ConfigExtractor.mappings(config);
    subscriptionPath = ConfigExtractor.subscriptionsPath(config, subscriptions);

    fieldTreeElements = SchemaGenerator.buildFieldTree(config);
    outputSchema = SchemaGenerator.generate(fieldTreeElements);

    this.plcConnection = ConnectionManager.getConnection(plcConnectionString);

    // Check if this connection support subscribing to data.
    if (!plcConnection.getMetadata().canSubscribe() || !plcConnection.getMetadata().canRead()) {
      log.error("This plc driver does not have read & subscribe capabilities");
      throw new IllegalStateException(
          "This plc driver does not have read & subscribe capabilities");
    }
  }

  public List<SourceRecord> getNewEvents() throws InterruptedException {
    var result = new ArrayList<SourceRecord>();

    while (!this.eventQueue.isEmpty()) {
      result.add(eventQueue.take());
    }

    return result;
  }

  public void close() {
    try {
      plcConnection.close();
    } catch (Exception e) {
      log.error("Fail to close PlcConnection", e);
    }
  }

  public void maybeSendFetches() {
    // For each tag
    for (String subscription : subscriptions) {
      if (!inflightMap.getOrDefault(subscription, false)) {
        inflightMap.put(subscription, true);
        sendFetchForSubscription(subscription)
            .thenRun(
                () -> {
                  inflightMap.put(subscription, false);
                });
      }
    }
  }

  public CompletableFuture sendFetchForSubscription(String subscription) {
    var subscriptionMapping = mappings.get(subscription);
    var subPath = subscriptionPath.get(subscription);

    var subField =
        subscriptionMapping.stream()
            .filter(pair -> pair.getValue().equals(subPath))
            .map(Pair::getKey)
            .findFirst()
            .get();

    // Sequential read for each field
    var readBuilder = plcConnection.readRequestBuilder();
    subscriptionMapping.forEach(
        mappingPair -> readBuilder.addItem(mappingPair.getKey(), mappingPair.getValue()));

    var asyncResponse = readBuilder.build().execute();

    asyncResponse.whenComplete(
        (response, throwable) -> {
          if (response != null) {

            var newValue = extractFieldFromResponse(response, subField);
            var currentValue = valueBuffer.get(subPath);

            // First Value is kept as memory an will be the first reference value
            if (currentValue == null) {
              valueBuffer.put(subPath, newValue);
              return;
            }

            // Only send an event when "subscribed" field change
            if (currentValue.equals(newValue)) {
              return;
            }

            valueBuffer.put(subPath, newValue);
            var sourceRecord =
                ResponseToRecordConverter.convert(
                    response,
                    kafkaTopic,
                    outputSchema,
                    subscriptionMapping,
                    keyComposition,
                    fieldTreeElements);
            try {
              eventQueue.put(sourceRecord);
            } catch (InterruptedException e) {
              log.error("Interrupted during event queue enrishment", e);
            }
          } else {
            log.error("Error during read request", throwable);
          }
        });
    return asyncResponse;
  }

  private Object extractFieldFromResponse(PlcReadResponse response, String key) {

    var fieldType = fieldTreeElements.get(key).getType();

    switch (fieldType) {
      case "FLOAT":
        return response.getFloat(key);
      case "STRING":
      default:
        return response.getString(key);
    }
  }
}
