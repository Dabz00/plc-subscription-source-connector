package com.cgi.connect.config;

import static com.cgi.connect.PLCSubscriptionSourceConfig.SUBSCRIPTIONS;

import com.cgi.connect.PLCSubscriptionSourceConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;

public class ConfigExtractor {

  /**
   * Extract mapping rules from configuration
   *
   * @param props the full configuration
   * @return Fields mapping
   */
  public static Map<String, List<Pair<String, String>>> mappings(
      PLCSubscriptionSourceConfig props) {
    var result = new HashMap<String, List<Pair<String, String>>>();

    // extract mapping from config
    props.originalsWithPrefix("plc.mappings.").entrySet().stream()
        .forEach(
            mappingPropertyEntry -> {
              var key = mappingPropertyEntry.getKey();
              var value = mappingPropertyEntry.getValue();

              var subscriptionName = key.split("\\.")[0];
              var fieldName = key.split("\\.")[1];

              var mappingForSubscription = result.get(subscriptionName);

              if (mappingForSubscription == null) {
                mappingForSubscription = new ArrayList<>();
              }

              mappingForSubscription.add(Pair.of(fieldName, value.toString()));

              result.put(subscriptionName, mappingForSubscription);
            });

    return result;
  }

  /**
   * Extract subscriptions
   *
   * @param props the full configuration
   * @return Fields mapping
   */
  public static Map<String, String> subscriptionsPath(
      PLCSubscriptionSourceConfig props, List<String> tags) {

    var result = new HashMap<String, String>();

    tags.forEach(
        tag -> {
          var path = props.originals().get(SUBSCRIPTIONS + "." + tag + ".path").toString();
          result.put(tag, path);
        });

    return result;
  }
}
